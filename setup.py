from setuptools import setup, find_packages

VERSION = "0.1"

setup(
    name="emailvision",
    version=VERSION,
    author="Julian Bez",
    author_email="julian@uhura.de",
    url="http://git.uhura.de/total/emailvision",
    description="""Emailvision integration component""",
    packages=find_packages(),
    namespace_packages=[],
    include_package_data=True,
    zip_safe=False,
    license="None",
    install_requires=['suds']
)
