from contextlib import contextmanager
from django.conf import settings
import suds
import requests
from requests.exceptions import Timeout


def campaigncommander_call(**kwargs):
    """
    This is a basic HTTP POST call that sends data to the campagin commander.
    The form was set up by total to collect the data and send a double-opt-in
    email to the user, which is otherwise not possible via the API.
    So no need to use the API when using this call.
    """
    base_url = "http://p2trc.emv2.com/D2UTF8"
    entries = {}
    entries['emv_tag'] = kwargs.pop("emv_tag", settings.CCOMMANDER_EMV_TAG)
    entries['emv_ref'] = kwargs.pop("emv_ref", settings.CCOMMANDER_EMV_REF)
    for k, v in kwargs.items():
        entries[k.upper() + "_FIELD"] = v
    try:
        r = requests.get(base_url, params=entries, timeout=1, allow_redirects=False)
    except Timeout:
        pass


class Remote(object):
    """Manages communication with the remote database through a SOAP
    webservice
    """
    @contextmanager
    def get_connection(self):
        client = suds.client.Client(self.wsdl)
        con = client.service.openApiConnection(settings.CCOMMANDER_API_USER,
                                               settings.CCOMMANDER_API_PASSWORD,
                                               settings.CCOMMANDER_API_KEY)
        try:
            yield client, con
        finally:
            client.service.closeApiConnection(con)


class MemberRemote(Remote):
    """Remote for Member model"""

    wsdl = settings.CCOMMANDER_API_MEMBER_UPDATE_WSDL

    def get_by_email(self, email):
        with self.get_connection() as (client, con):
            print client.service.getMemberByEmail(con, email)

    def getlist(self):
        with self.get_connection() as (client, con):
            print client.service.getListMembersByPage(con, 1)

    def save(self, **kwargs):
        with self.get_connection() as (client, con):
            s = client.factory.create('synchroMember')
            s.email = kwargs.pop("email")
            s.memberUID = 'email:%s' % s.email
            entries = []
            for k, v in kwargs.items():
                entries.append({'key': k.upper().replace("_", ""),
                                'value': v})
            s.dynContent.entry.extend(entries)
            client.service.insertOrUpdateMemberByObj(con, s)
